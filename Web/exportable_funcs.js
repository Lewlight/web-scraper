let mysql =  require("mysql");


//This method checks if the ISBN is valid.
function checkIfIsbn(input){
    if(input.length != 13){
        return false;
    }
    for(let i =0; i < input.length; i++){
        // if any character is not a number we return false.
        let char = input[i];
        if(isNaN(char)){
            return false;
        }
    }
    return true;
}

async function getFormats(book, connectionPool){
    // This method takes a book and gets the formats available for this book.
    book.formats = "";
    let formatPromise = new Promise((resolve, reject) =>{
        //This is the query for getting book formats.
        let selectQuery = "SELECT format FROM format WHERE id='" + book.id + "';";
        connectionPool.query(selectQuery, (err, result) =>{
            if(err){
                reject(JSON.stringify(err));
            }
            else{
                resolve(result);
            }
        });
    });
    try{
        // we add the formats found to the book object inputted
        let formatQueryResult = await formatPromise;
        for(let j = 0; j < formatQueryResult.length; j++){
            book.formats = book.formats +" "+ formatQueryResult[j].format;
        }
    }
    catch(err){
        console.log("found an error in format");
        console.log(err);
    }
    // we also return the modified book.
    return book;
}

async function getProductsFromBookId(bookId, connectionPool){
    //This method returns the sellers informatin for a given book id.
    let promise = new Promise((resolve, reject) =>{
        let selectQuery = "SELECT product.link, product.price, product.isbn, product.website, format.format FROM product, format WHERE " + bookId +" = format.id AND format.isbn = product.isbn;";
        connectionPool.query(selectQuery, (err, result) =>{
            if(err){
                reject(JSON.stringify(err));
            }
            else{
                resolve(result);
            }
        });
    });
    try{
        // we return the list of products for a given book.
        let queryResult = await promise;
        return queryResult;        
    }
    catch(err){
        console.log("error");
        console.log(err);
    }
}

async function getBookFromId(bookId, connectionPool){
    // This gets a book for a given id.
    let promise = new Promise((resolve, reject) =>{
        let selectQuery = "SELECT * FROM book WHERE " + bookId +" = id;";
        connectionPool.query(selectQuery, (err, result) =>{
            if(err){
                reject(JSON.stringify(err));
            }
            else{
                resolve(result);
            }
        });
    });
    try{
        let queryResult = await promise;
        return queryResult[0];        
    }
    catch(err){
        console.log("error");
        console.log(err);
    }
}

function attachProducts(book, products) {
    // This attaches a book object with a list of products (sellers) of this book
    let books = {};
    books.bookInfo = book;
    books.products = [];
    for(let k = 0; k < products.length; k++){
        let product = products[k];
        books.products.push(product);
    }
    return books;
}

module.exports.getBookFromId = getBookFromId;
module.exports.getFormats = getFormats;
module.exports.getProductsFromBookId = getProductsFromBookId;
module.exports.checkIfIsbn = checkIfIsbn;
module.exports.attachProducts = attachProducts;
