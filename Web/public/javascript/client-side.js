// array of products found from recent search
let pageProducts = [];
let maximumPageIndex = 0;
// index of the current page.
let pageIndex = 0;
let nextPageIndex = 1;
let previousPageIndex = 0;
let lastQuery = "action";

window.onload = () =>{
    //When the page loads we show action books, my favourite books.
    getGenres("action");
    loadPage();
};
function loadPreviousPage(){
    if(pageIndex != 0){
        pageIndex = Math.max(pageIndex-2, 0);
    }
    loadPage();
}
//This function loads the next page from the search found.
function loadPage(){
    // we check if the page index is at the end so we can reset the page index.
    // this is so when we try to get the next page at the end of the list
    // of the products, we will show the first page of products.

    //If there are no products we display a message
    if(pageProducts.length == 0){
        document.getElementById("productDisplayWindow").innerHTML = "<h1>No Results Found</h1>";
    }
    else{
        // we display the page number
        let content = '<h2 id="pageNumber">Page Number: ' + (pageIndex + 1) +'</h2>';
        // we will have three rows of products.
        for(let rowNum = 0; rowNum < 3; rowNum++){
            content += '<div class="productDisplayRow">';
            // each row will have 4 products
            for(let k = rowNum*4; k < (rowNum+1)*4; k++){
                if (k == pageProducts.length){
                    // if we reach the end of the products we stop looping.
                    content += '</div>';
                    content += '<div><button onclick="searchBooks(undefined, ' + previousPageIndex +')">Previous Page</button><button onclick="searchBooks(undefined, ' + nextPageIndex +')">Next Page</button></div>';
                    document.getElementById("productDisplayWindow").innerHTML = content;
                    return;
                }
                // We add the book information to the html.
                let bookInfo = pageProducts[k];
                content += '<div class="productDisplay"><img width="250px" height="310px" src="' + bookInfo.image_link + '"><p class="titleDisplay">' + bookInfo.title + '</p>';
                content += '<p class="authorDisplay">' + bookInfo.author + '</p><p class="genreDisplay">' + bookInfo.genre + '</p>';
                content += '<p class="formatDisplay">' + bookInfo.formats + '</p><button onclick=getBookSellers("' + bookInfo.id +'")>Compare</button></div>';
            }
            content += '</div>'
        }
        // we update the html.
        content += '<div><button onclick="searchBooks(undefined, ' + previousPageIndex +')">Previous Page</button><button onclick="searchBooks(undefined, ' + nextPageIndex +')">Next Page</button></div>';
        document.getElementById("productDisplayWindow").innerHTML = content;
    }
}

// A method that gets the books of a specific genre.
function getGenres(genre){
    searchBooks(genre, 0);
}

// this simple method extracts the search text inputted.
function getSearchInput(){
    return document.getElementsByTagName("input")[0].value;
}

// This method sends the search input to get the books that match the search.
function searchBooks(searchQuery, index){
    let searchInput = "";
    if (searchQuery === undefined){
        searchInput = lastQuery;
    }
    else if(searchQuery === 0){
        searchInput = getSearchInput();
        lastQuery = searchInput;
    }
    else{
        searchInput = searchQuery;
        lastQuery = searchInput;
    }
    let request = new XMLHttpRequest();
    request.onload = () => {
        if (request.status === 200) {
            let books = request.responseText;
            let parsedData = JSON.parse(books);
            pageProducts = parsedData["books"];
            maximumPageIndex = parsedData["pageNumberMax"];
            previousPageIndex = Math.max(0, index - 1);
            nextPageIndex = Math.min(maximumPageIndex, index + 1);
            pageIndex = index;
            loadPage();
        }
    }
    request.open("GET", "/search/" + searchInput + "?page_number=" + index, true);
    request.setRequestHeader("Content-type", "application/json");
    request.send();
}

// This gets the specific book sellers to display them and compare them.
function getBookSellers(bookId){
    let request = new XMLHttpRequest();
    request.onload = () => {
        if (request.status === 200) {
            let products = request.responseText;
            displayBookSellers(JSON.parse(products));
        }
    }
    request.open("GET", "/product/" + bookId, true);
    
    request.setRequestHeader("Content-type", "application/json");
    request.send();
}

// This updates the html content to show the single product chosen info.
function displayBookSellers(listSellers){
    let bookInfo = listSellers.bookInfo;
    let productList = listSellers.products;
    content = '<div id="productComparison"><div id ="productInfo"><img src="' + bookInfo.image_link + '" id="productInfoImage">';
    content += '<p class="productTitle">' + bookInfo.title + '</p><p class="productAuthor">' + bookInfo.author + '</p>';
    content += '<p class="productGenre">' + bookInfo.genre + '</p></div><div id="comparisonInfo"><p>Sellers Available:</p>';
    for(let k = 0; k < productList.length; k++){
        let product = productList[k];
        content += '<div class="providerInfo"><span class="websiteInfo">' + product.website + '</span><span class="priceInfo">' + product.price + '£</span>';
        content += '<span class="formatInfo">' + product.format + '</span><span class="isbnInfo">' + product.isbn + '</span>';
        content += '<a href="' + product.link +'"><button>HERE</button></a></div>'
    }
    document.getElementById("productDisplayWindow").innerHTML = content;
    document.getElementById("nextPageButton").innerHTML = "";
}