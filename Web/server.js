
const { request } = require('express');
let express = require('express');
let mysql = require('mysql');
let url = require('url');
let bodyParser = require('body-parser');
let funcs = require('./exportable_funcs');

let connectionPool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'book_scraper',
    debug: false
});


let app = express();
app.use(bodyParser.json());
app.use(express.static('public'));

//The different requestes of the server.
app.get("/search/*", getHandler);
app.get("/books", getHandler);
app.get("/product/*", getHandler);

//This will handle the request of the web services to access the database.
function getHandler(request, response){
    // we parse the various parameters for the data to be returned.
    let parsedUrl = url.parse(request.url, true);
    let parameters = parsedUrl.query;
    let maxBooks = parameters.max_books;
    let genre = parameters.genre;
    let author = parameters.author;
    let page_number = parameters.page_number;
    let splitPath = parsedUrl.pathname.split("/");
    if (splitPath[1] === "books"){
        allBooks(request, response, maxBooks, genre, author);
    }
    else if(splitPath[1] === "search" && splitPath.length >=3 && splitPath[2] !== ""){
        booksHandler(request, response, splitPath[2], page_number);
    }
    else if(splitPath[1] === "search"){
        response.send('{"books": [], "message": "A search input needs to be specified"}');
    }
    else if(splitPath[1] === "product" && splitPath.length >=3 && splitPath[2] !== ""){
        getBookSellersHandler(request, response, splitPath[2]);
    }
    else if(splitPath[1] === "product"){
        response.send('{"books": [], "message": "The book id needs to be specified."}');
    }
    else{
        response.send('{"error": "Path for get request is not recognized"}');
    }
}

// This extracts books data form the database for the web services.
async function allBooks(request, response, maxBooks, genre, author){
    let promise = new Promise((resolve, reject) =>{
        let selectQuery = "SELECT * FROM book "
        let whereAdded = false;
        // depending on the parameters given we have a different query for the database.
        if(genre !== undefined){
            selectQuery += " WHERE genre='" + genre + "'";
            whereAdded = true;
        }
        if(author !== undefined){
            if(!whereAdded){
                selectQuery += " WHERE ";
            }
            else{
                selectQuery += " AND ";
            }
            selectQuery += " author='" + author + "'";
        }
        if(maxBooks !== undefined){
            selectQuery += " LIMIT 0, " + maxBooks;
        }
        selectQuery += ";";
        connectionPool.query(selectQuery, (err, result) =>{
            if(err){
                reject(JSON.stringify(err));
            }
            else{
                resolve(result);
            }
        });
        
    });
    try{
        let books = [];
        let queryResult = await promise;
        for(let k = 0; k < queryResult.length; k++){
            let book = queryResult[k];
            // we get also the formats of the books, and the sellers.
            await funcs.getFormats(book, connectionPool);
            let products = await funcs.getProductsFromBookId(book.id, connectionPool);
            books.push(funcs.attachProducts(book, products));
        }
        response.send(JSON.stringify(books));
    }
    catch(err){
        console.log("error");
        console.log(err);
    }
}

// This handles the request to get the books and their sellers.
async function getBookSellersHandler(request, response, bookId){
    let book = await funcs.getBookFromId(bookId, connectionPool);
    let products = await funcs.getProductsFromBookId(bookId, connectionPool);
    response.send(JSON.stringify(funcs.attachProducts(book, products)));
}

// This handles the search request.
async function booksHandler(request, response, searchInput, page_number){
    // if no page number was specified we returnt he first page.
    if (page_number === undefined){
        page_number = 0;
    }
    let books = [];
    let promise = new Promise((resolve, reject) =>{
        let selectQuery = "SELECT * FROM book WHERE title LIKE '%"+ searchInput+"%' OR author LIKE '%" + searchInput + "%' OR upper(genre)=upper('" + searchInput + "')";

        // if the serach input is a valid ISBN we search for a book with a specific ISBN.
        if(funcs.checkIfIsbn(searchInput)){
            selectQuery = "SELECT book.id, title, genre, author, image_link FROM book, format WHERE book.id=format.id AND format.isbn='" + searchInput + "'";
        }
        // This is added to handle the pagination within the request itself, since we want
        // to display at maximum 12 products in a page.
        selectQuery += " LIMIT 12 OFFSET " + page_number*12 + ";"
        //This is the quesry for getting book information.
        connectionPool.query(selectQuery, (err, result) =>{
            if(err){
                reject(JSON.stringify(err));
            }
            else{
                resolve(result);
            }
        });
    });
    // we use this to get the number of books for a certain search input.
    let countPromise = new Promise((resolve, reject) =>{
        let selectQuery = "SELECT COUNT(book.id) FROM book WHERE title LIKE '%"+ searchInput+"%' OR author LIKE '%" + searchInput + "%' OR upper(genre)=upper('" + searchInput + "');";
        // if the serach input is a valid ISBN we search for a book with a specific ISBN.
        if(funcs.checkIfIsbn(searchInput)){
            selectQuery = "SELECT COUNT(book.id) FROM book, format WHERE book.id=format.id AND format.isbn='" + searchInput + "';";
        }
        //This is the quesry for getting book information.
        connectionPool.query(selectQuery, (err, result) =>{
            if(err){
                reject(JSON.stringify(err));
            }
            else{
                resolve(result);
            }
        });
    });
    try{
        let queryResult = await promise;
        let productCount = await countPromise;
        for(let k = 0; k < queryResult.length; k++){
            let book = queryResult[k];
            // once we have the books we get their formats.
            await funcs.getFormats(book, connectionPool);
            books.push(book);
        }
        // we return only 12 books depending on the page number needed.
        let responseData = {"books": queryResult};
        // we return the number of pages the server has available it helps with limiting the possible
        // pages the client can request.
        responseData["pageNumberMax"] = Math.floor(productCount[0]["COUNT(book.id)"]/12);
        response.send(JSON.stringify(responseData));
    }
    catch(err){
        console.log("error");
        console.log(err);
    }
}


// listen for requests.
app.listen(8000);