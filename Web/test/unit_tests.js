let funcs = require("../exportable_funcs");
let mysql = require('mysql');
let chai = require('chai');

let expect = chai.expect;
let assert = chai.assert;

describe('#testIsbnValidator', () => {
    it('should check the ISBN is valid', (done)=>{
        // we compare different possible values for this method.
        assert.isTrue(funcs.checkIfIsbn("9999999999999"));
        assert.isFalse(funcs.checkIfIsbn(""));
        assert.isFalse(funcs.checkIfIsbn("4444"));
        assert.isFalse(funcs.checkIfIsbn("111111111111111111111"));
        assert.isFalse(funcs.checkIfIsbn("abcdefghijklm"));
        assert.isFalse(funcs.checkIfIsbn("111111111111b"));
        done();
    })
});

describe('#testInfoToProductAttchor', () => {
    it('attachs list of book products to the book info ', (done)=>{
        // we create dummy object and arrays to check if the method 
        //correctly attaches the list of the products and the bookInfo together.
        let bookInfo = {"title":"Generic", "author":"any"};
        let products=[{"link":"example1.com", "isbn":"1111111111111"},
        {"link":"example2.com", "isbn":"2222222222222"},
        {"link":"example3.com", "isbn":"3333333333333"}];
        let compared = {"bookInfo":bookInfo, "products": products};
        let attached = funcs.attachProducts(bookInfo, products);
        expect(attached.products.length).to.equal(3);
        expect(attached).to.deep.equal(compared);
        done();
    });
});

describe('#testingDatabase', () => {
    // for the database tests we will create a connection pool for each test.
    // to avoid overlap. Dummy data was already entered to the database prior to the tests.
    describe('#testGettingBookWithId', () => {
        it('it should retrieve the dummy row from the book table in the database', async ()=>{
            let connectionPool = mysql.createPool({
                connectionLimit: 5,
                host: "localhost",
                user: "root",
                password: "",
                database: "book_scraper",
                debug: false
            });
            // We check if the already entered single book row exists.
            let compared = {"id":-1, "title":"generic", "genre":"generic", "author":"any", "image_link":"example.com"};
            let info = await funcs.getBookFromId(-1, connectionPool);
            connectionPool.end();
            assert.deepEqual(info, compared);
        });
    });
    
    describe('#testGettingformatsWithId', () => {
        it('it should retrieve the dummy row from the database', async ()=>{
            let connectionPool = mysql.createPool({
                connectionLimit: 5,
                host: "localhost",
                user: "root",
                password: "",
                database: "book_scraper",
                debug: false
            });
            let book = {"id": -1};
            let bookFound = await funcs.getFormats(book, connectionPool);
            connectionPool.end();
            let compared = {"id":-1, "formats":" Paperback Hardback"};
            // we check if the formats of a given book are found correctly.
            assert.equal(book.id, compared.id);
            // we seperate the string of formats and put it into a set to compare the sets
            // to avoid any differences of the order with which the database might respond with.
            assert.deepEqual(new Set(book.formats.split(" ")), new Set(compared.formats.split(" ")));
            assert.deepEqual(new Set(bookFound.formats.split(" ")), new Set(compared.formats.split(" ")));
        });
    });
    
    describe('#testGettingProductsFromBookId', () => {
        it('it should retrieve the dummy row from the products table that are linked to the bookId specified in the database', async ()=>{
            let connectionPool = mysql.createPool({
                connectionLimit: 5,
                host: "localhost",
                user: "root",
                password: "",
                database: "book_scraper",
                debug: false
            });
            let productsFound = await funcs.getProductsFromBookId(-1, connectionPool);
            connectionPool.end();
            // we get the products connected to a specific book already inputted into the database.
            let compared = [{"link":"example1.com", "price":0.0, "isbn":"0000000000001", "website":"web1.com", "format":"Hardback"},
            {"link":"example2.com", "price":1.0, "isbn":"0000000000001", "website":"web2.com", "format":"Hardback"},
            {"link":"example3.com", "price":2.0, "isbn":"0000000000002", "website":"web3.com", "format":"Paperback"}];
            assert.deepEqual(compared, productsFound);
        });
    });
});
