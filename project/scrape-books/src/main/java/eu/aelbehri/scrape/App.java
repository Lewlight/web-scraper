package eu.aelbehri.scrape;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * App class containing the main method where the program starts.
 * @author Ayman El Behri
 */
public class App 
{
    /**
     * main method where the execution of the program starts.
     * @param args 
     */
    public static void main( String[] args )
    {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
   
        //Get ScraperManager bean
        ScraperManager scraperManager = (ScraperManager) context.getBean("scraperManager");
        scraperManager.startScraping();
        
        System.exit(0);
    }
}
