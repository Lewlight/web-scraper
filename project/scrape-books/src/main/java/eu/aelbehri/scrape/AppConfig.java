package eu.aelbehri.scrape;

import org.springframework.context.annotation.*;
import org.hibernate.*;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

/**
 * This is the configuration class where we can define and extract beans
 * for the program to run.
 * @author Ayman El Behri
 */
@Configuration
public class AppConfig {
    
    private SessionFactory sessionFactory;
    
    /**
     * this method is used to get the bean of ScraperManager.
     * @return ScraperManager
     */
    @Bean
    public ScraperManager scraperManager(){
        ScraperManager returned = new ScraperManager();
        sessionFactoryGenerator();
        returned.setSessionFactory(sessionFactory);
        returned.addScraper(worderyScraper());
        returned.addScraper(waterstonesScraper());
        returned.addScraper(secondWaterstonesScraper());
        returned.addScraper(foylesScraper());
        returned.addScraper(shakespeareScraper());
        returned.addScraper(bookshopScraper());
        return returned;
    }
    
    /**
     * this method is used to get a bean of WorderyScraper
     * which is injected in scraper manager.
     * @return WorderyScraper
     */
    @Bean
    public WorderyScraper worderyScraper(){
        WorderyScraper scraper = new WorderyScraper();
        String[] searchGenres = {"fantasy", "action", "drama", "science fiction"};
        scraper.setSearchGenres(searchGenres);
        return scraper;
    }
    
    /**
     * this method is used to get a bean of WaterstonesScraper used to get format
     * 16 (Hardback) which is injected in scraper manager.
     * @return WaterstonesScraper
     */
    @Bean public WaterstonesScraper waterstonesScraper(){
        WaterstonesScraper waScraper = new WaterstonesScraper();
        waScraper.setLink("https://www.waterstones.com/books/search/term/%s/format/16");
        String[] searchGenres = {"fantasy", "action", "drama", "science fiction"};
        waScraper.setSearchGenres(searchGenres);
        return waScraper;
    }
    
    /**
     * this method is used to get a bean of WaterstonesScraper used to get format
     * 17 (Paperback) which is injected in scraper manager.
     * @return WaterstonesScraper
     */
    @Bean public WaterstonesScraper secondWaterstonesScraper(){
        WaterstonesScraper waScraper = new WaterstonesScraper();
        waScraper.setLink("https://www.waterstones.com/books/search/term/%s/format/17");
        String[] searchGenres = {"fantasy", "action", "drama", "science fiction"};
        waScraper.setSearchGenres(searchGenres);
        return waScraper;
    }
    
    /**
     * this method is used to get a bean of FoylesScraper
     * which is injected in scraper manager.
     * @return FoylesScraper
     */
    @Bean
    public FoylesScraper foylesScraper(){
        FoylesScraper scraper = new FoylesScraper();
        String[] searchGenres = {"fantasy", "action", "drama", "science fiction"};
        scraper.setSearchGenres(searchGenres);
        return scraper;
    }
    
    /**
     * this method is used to get a bean of ShakespeareScraper
     * which is injected in scraper manager.
     * @return ShakespeareScraper
     */
    @Bean
    public ShakespeareScraper shakespeareScraper(){
        ShakespeareScraper scraper = new ShakespeareScraper();
        String[] searchGenres = {"fantasy", "action", "drama", "science fiction"};
        scraper.setSearchGenres(searchGenres);
        return scraper;
    }
    
    /**
     * this method is used to get a bean of BookshopScraper
     * which is injected in scraper manager.
     * @return BookshopScraper
     */
    @Bean
    public BookshopScraper bookshopScraper(){
        BookshopScraper scraper = new BookshopScraper();
        String[] searchGenres = {"fantasy", "action", "drama", "science fiction"};
        scraper.setSearchGenres(searchGenres);
        return scraper;
    }
    
    /**
     * This creates a SessionFactory object which is used to connect to the
     * database, if not created already.
     */
    public void sessionFactoryGenerator(){
        if(sessionFactory == null){
            try{
                StandardServiceRegistryBuilder standardServiceRegistryBuilder = new StandardServiceRegistryBuilder();

                //Load configuration from hibernate configuration file.
                //Here we are using a configuration file that specifies Java annotations.
                standardServiceRegistryBuilder.configure("hibernate.cfg.xml"); 

                //Create the registry that will be used to build the session factory
                StandardServiceRegistry registry = standardServiceRegistryBuilder.build();
                try {
                    //Create the session factory - this is the goal of the init method.
                    this.sessionFactory = new MetadataSources( registry ).buildMetadata().buildSessionFactory();
                }
                catch (Exception e) {
                        /* The registry would be destroyed by the SessionFactory, 
                            but we had trouble building the SessionFactory, so destroy it manually */
                        System.err.println("Session Factory build failed.");
                        e.printStackTrace();
                        StandardServiceRegistryBuilder.destroy( registry );
                }
                //Ouput result
                System.out.println("Session factory built.");
            }
            catch (Throwable ex) {
                // Make sure you log the exception, as it might be swallowed
                System.err.println("SessionFactory creation failed." + ex);
                ex.printStackTrace();
                try{
                    java.nio.file.Path currentRelativePath = java.nio.file.Paths.get("");
                    String s = currentRelativePath.toAbsolutePath().toString();
                    System.out.println("Current absolute path is: " + s);
                }
                catch(Exception x){
                    
                }
                System.exit(0);
            }
        }
    }
}
