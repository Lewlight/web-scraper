package eu.aelbehri.scrape;

import javax.persistence.*;

/**
 * Book class to match the SQL database table.
 * @author Ayman El Behri
 */
@Entity
@Table(name="book")
public class Book {
    @Column(name = "title")
    private String title;
    
    @Column(name = "author")
    private String author;
    
    @Column(name = "genre")
    private String genre;
    
    @Column(name = "image_link")
    private String imageLink;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    /**
     * No argument constructor.
     */
    public Book() {}

    /**
     * @return Book id
     */
    public int getId() {
        return id;
    }

    /**
     * This sets the id of the book
     * @param id Book id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return Book title
     */
    public String getTitle() {
        return title;
    }

    /**
     * This sets the title of the book
     * @param title Book title.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return Book author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * This sets the author of the book
     * @param author Book author.
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * @return Book genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * This sets the genre of the book
     * @param genre Book genre.
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     * @return Book imageLink
     */
    public String getImageLink() {
        return imageLink;
    }

    /**
     * This sets the image link of the book
     * @param imageLink Book image link.
     */
    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
    
    
    
}
