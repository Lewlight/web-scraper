package eu.aelbehri.scrape;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import java.util.List;

/**
 * This class is used to scrape Bookshop.org.
 * @author Ayman El Behri
 */
public class BookshopScraper extends WebScraper{
    
    /**
     * No argument constructor sets up the search link for this website.
     */
    public BookshopScraper(){
        setLink("https://uk.bookshop.org/books?keywords=%s");
    }
    
    /**
     * This method is used to start the thread that scrapes the data,
     * and then calls the method that sends the data to the database.
     */
    @Override
    public void run(){
        //we set up the driver to access the sites data.
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);

        WebDriver driver = new ChromeDriver(options);

        for(String genre: getSearchGenres()){
            driver.get(String.format(getLink(), genre));

            //Wait for page to load
            try {
                Thread.sleep(getTimeout());
            }
            catch(Exception ex){
                ex.printStackTrace();
            }

            //search for all book html blocks
            List<WebElement> bookList = driver.findElements(By.className("booklist-book"));
            for (WebElement book : bookList) {
                try{
                    String imageLink = book.findElement(By.tagName("img")).getAttribute("src");
                    String link = book.findElement(By.tagName("h2")).findElement(By.tagName("a")).getAttribute("href");
                    String isbn = getIsbn(link);
                    if(!validateIsbn(isbn)) continue;
                    String title = book.findElement(By.tagName("h2")).findElement(By.tagName("a")).getText();
                    String author = book.findElement(By.tagName("h3")).getText();
                    String price = book.findElement(By.className("font-sans-bold")).getText();
                    String format = "Not Found"; //can't scrape the format of the book from search page.

                    //we create the objs needed to create the columns in the database.
                    Book bookFound = new Book();
                    bookFound.setAuthor(removeIllegalCharacters(author.trim()));
                    bookFound.setGenre(genre);
                    bookFound.setTitle(removeIllegalCharacters(title.trim()));

                    Format formatFound = new Format();
                    formatFound.setIsbn(isbn.trim());
                    formatFound.setFormat(format.trim());

                    Product productFound = new Product();
                    productFound.setLink(link.trim());
                    productFound.setImageLink(imageLink.trim());
                    productFound.setWebsite("Bookshop.org");
                    productFound.setIsbn(isbn.trim());
                    productFound.setPrice(filterPrice(price));
                    //send the data to the database.
                    saveData(bookFound, formatFound, productFound);
                }
                // if an exception occurs we just move on to the next product.
                catch(Exception ex){
                    continue;
                }            
            }
        }

        driver.quit();
    }
    
    /**
     * This method extracts the isbn from the products link.
     * @param link used that contains the isbn code
     * @return Isbn of the product.
     */
    public String getIsbn(String link){
        return link.substring(link.length() - 13, link.length());
    }
}
