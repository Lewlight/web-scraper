package eu.aelbehri.scrape;

import javax.persistence.*;

/**
 * Format class to match the format table in the SQL database.
 * @author Ayman El Behri
 */
@Entity
@Table(name="format")
public class Format {
    @Id
    @Column(name="isbn")
    private String isbn;
    
    @Column(name="format")
    private String format;
    
    @Column(name="id")
    private int id;
    
    /**
     * No argument constructor.
     */
    public Format() {}

    /**
     * @return the book id this format is referring to
     */
    public int getId() {
        return id;
    }

    /**
     * This sets the id of the book this format refers to
     * @param id Book id.
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
     * @return the Isbn of this book format
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * This sets Isbn of this format
     * @param isbn format Isbn.
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    /**
     * @return the format type of this book format.
     */
    public String getFormat() {
        return format;
    }

    /**
     * This sets the format type of the format
     * @param format format type.
     */
    public void setFormat(String format) {
        this.format = format;
    }   
}
