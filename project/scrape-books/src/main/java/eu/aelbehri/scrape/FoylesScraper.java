package eu.aelbehri.scrape;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import java.util.List;

/**
 * This class is used to scrape Foyles.co.uk.
 * @author Ayman El Behri
 */
public class FoylesScraper extends WebScraper{
    
    /**
     * No argument constructor sets up the search link for this website.
     */
    public FoylesScraper(){
        setLink("https://www.foyles.co.uk/all?term=%s&top=200&ffor=Books");
    }
    
    /**
     * This method is used to start the thread that scrapes the data,
     * and then calls the method that sends the data to the database.
     */
    @Override
    public void run(){
        //we set up the driver to access the sites data.
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);

        WebDriver driver = new ChromeDriver(options);

        for(String genre: getSearchGenres()){
            driver.get(String.format(getLink(), genre));

            //Wait for page to load
            try {
                Thread.sleep(getTimeout());
            }
            catch(Exception ex){
                ex.printStackTrace();
            }

            //search for all book html blocks
            List<WebElement> bookList = driver.findElements(By.className("NewSRItem"));
            for (WebElement book : bookList) {
                try{
                    //get the products information
                    String imageLink = book.findElement(By.tagName("img")).getAttribute("src");
                    String isbn = book.getAttribute("data-ean");
                    if(!validateIsbn(isbn)) continue;
                    String link = book.findElement(By.className("Title")).getAttribute("href");
                    String title = book.findElement(By.className("Title")).getText();
                    String author = book.findElement(By.className("Author")).getText();
                    String price = book.findElement(By.className("CellOnlinePrice")).getText();
                    String format = book.findElement(By.className("BookType")).getText();

                    //We create the objects from the data
                    Book bookFound = new Book();
                    bookFound.setAuthor(removeIllegalCharacters(author.trim()));
                    bookFound.setGenre(genre);
                    bookFound.setTitle(removeIllegalCharacters(title.trim()));

                    Format formatFound = new Format();
                    formatFound.setIsbn(isbn.trim());
                    formatFound.setFormat(format.trim());

                    Product productFound = new Product();
                    productFound.setLink(link.trim());
                    productFound.setImageLink(imageLink.trim());
                    productFound.setWebsite("Foyles.co.uk");
                    productFound.setIsbn(isbn.trim());
                    productFound.setPrice(filterPrice(price));
                    //send data to the database
                    saveData(bookFound, formatFound, productFound);
                }
                catch(Exception ex){
                    //We just ignore the cases that cause any exceptions
                    continue;
                }            
            }
        }

        driver.quit();
    }
}
