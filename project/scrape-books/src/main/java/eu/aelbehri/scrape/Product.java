package eu.aelbehri.scrape;

import javax.persistence.*;

/**
 * Product class to match the product table in the SQL database.
 * Containing the information of the product scraped.
 * @author Ayman El Behri
 */
@Entity
@Table(name="product")
public class Product {
    @Id
    @Column(name="link")
    private String link;
    
    @Column(name="image_link")
    private String imageLink;
    
    @Column(name="website")
    private String website;
    
    @Column(name="isbn")
    private String isbn;
    
    @Column(name="price")
    private double price;

    /**
     * No argument constructor.
     */
    public Product() {}

    /**
     * @return the link of this product.
     */
    public String getLink() {
        return link;
    }

    /**
     * This sets the link of this product
     * @param link product link.
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return the image link of this product
     */
    public String getImageLink() {
        return imageLink;
    }

    /**
     * This sets the image link of this product
     * @param imageLink product image link.
     */
    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    /**
     * @return the Website this product was scraped from.
     */
    public String getWebsite() {
        return website;
    }

    /**
     * This sets the Website this product was scraped from.
     * @param website product origin Website.
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     * @return the price of this product
     */
    public double getPrice() {
        return price;
    }

    /**
     * This sets the price of this product
     * @param price product price.
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the Isbn of this product
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * This sets the Isbn of this product
     * @param isbn product Isbn.
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
    
    
    
}
