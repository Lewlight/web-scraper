package eu.aelbehri.scrape;

import java.util.ArrayList;
import org.hibernate.*;

/**
 * This class manages all the scrapers and starts the threads.
 * @author Ayman El Behri
 */
public class ScraperManager {
    private ArrayList<WebScraper> scrapers;
    private String[] genres;
    private SessionFactory sessionFactory;

    /**
     * No argument constructor.
     */
    public ScraperManager() {
        this.scrapers = new ArrayList();
    }

    /**
     * @return the SessionFactory used to connect to the SQL database.
     */
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * @param sessionFactory used to connect to the Database.
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    /**
     * Add scraper to the list of scrapers to start.
     * @param scraper The scraper to be added to the list
     */
    public void addScraper(WebScraper scraper){
        scrapers.add(scraper);
    }
    
    /**
     * This method starts scraping the websites using the genres.
     * it starts one thread for each website for each genre, and waits
     * for the threads to end before we move on to the other genre.
     */
    public void startScraping(){
        //we start all of the threads.
        for(WebScraper scraper: scrapers){
            scraper.setSessionFactory(sessionFactory);
            scraper.start();
        }
        //we wait for all of them to terminate.
        while(true){
            //We give some waiting time before checking if the set of scrapers is done.
            try{
                Thread.sleep(500);
            }
            catch(InterruptedException ex){
                System.out.println(ex.getMessage());
                break;
            }
            boolean allDone = true;
            for(WebScraper scraper: scrapers){
                if(scraper.isAlive()){
                    allDone = false;
                }
            }
            if (allDone){
                break;
            }
        }
        //we clear the current list of scrapers before we move on to the 
        //next genre.
        scrapers.clear();
        
        System.out.println("All the threads finished scraping, and storing the data.");
    }
}
