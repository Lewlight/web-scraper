package eu.aelbehri.scrape;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * This class is used to scrape shakespeareandcompany.com
 * @author Ayman El Behri
 */
public class ShakespeareScraper extends WebScraper{
    
    /**
     * No argument constructor
     */
    public ShakespeareScraper(){
        setLink("https://shakespeareandcompany.com/search?q=%s&type=books");
    }
    
    /**
     * This method is used to start the thread that scrapes the data,
     * and then calls the method that sends the data to the database.
     */
    @Override
    public void run(){
        //we set up the driver to access the sites data.
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(false);

        WebDriver driver = new ChromeDriver(options);

        for(String genre: getSearchGenres()){
            driver.get(String.format(getLink(), genre));
            //to load more books from one search we scroll to the bottom twice.
            JavascriptExecutor jse = (JavascriptExecutor)driver;
            jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
            try {
                Thread.sleep(getTimeout());
            }
            catch(Exception ex){
                ex.printStackTrace();
            }

            jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
            try {
                Thread.sleep(getTimeout());
            }
            catch(Exception ex){
                ex.printStackTrace();
            }

            //search for all book html blocks
            List<WebElement> bookList = driver.findElements(By.className("ng-scope"));
            //we remove the first element since this search page has an html block
            //that has the same class but is not a product and that is first in the page.
            bookList.remove(0);
            System.out.println("books found " + bookList.size());
            for (WebElement book : bookList) {
                try{
                    String imageLink = book.findElement(By.tagName("img")).getAttribute("src");
                    String isbn = getIsbn(imageLink);
                    //This is for books that have no images we ignore these books.
                    if(!validateIsbn(isbn)) continue;
                    String link = book.findElement(By.tagName("a")).getAttribute("href");
                    //This link should contain the isbn for it to work.
                    if(!link.contains(isbn)) continue;
                    String title = book.findElement(By.tagName("h2")).getText();
                    String author = book.findElement(By.tagName("h3")).getText();
                    String price = book.findElement(By.className("price")).getText();
                    String format = book.findElement(By.className("formcode")).getText();

                    //create objects from data.
                    Book bookFound = new Book();
                    bookFound.setAuthor(removeIllegalCharacters(author.trim()));
                    bookFound.setGenre(genre);
                    bookFound.setTitle(removeIllegalCharacters(title.trim()));

                    Format formatFound = new Format();
                    formatFound.setIsbn(isbn.trim());
                    formatFound.setFormat(format.trim());

                    Product productFound = new Product();
                    productFound.setLink(link.trim());
                    productFound.setImageLink(imageLink.trim());
                    productFound.setWebsite("shakespeareandcompany.com");
                    productFound.setIsbn(isbn.trim());
                    productFound.setPrice(filterPrice(price));

                    //send data to the database.
                    saveData(bookFound, formatFound, productFound);
                }
                catch(Exception ex){
                    //move on to next item if exception occurs.
                    continue;
                }            
            }
        }

        driver.quit();
    }
    
    /**
     * extracts Isbn from products link.
     * @param link
     * @return Isbn if product
     */
    public String getIsbn(String link){
        return link.substring(link.length() - 17, link.length() - 4);
    }
    
    /**
     * extracts the value from the string then does a simple conversion to pounds
     * to make comparison accurate.
     * @param price string.
     * @return the price value converted to pound.
     */
    @Override
    public double filterPrice(String price){
        double converted = 0.85*Double.parseDouble(price.split(" ", 2)[0]);
        String convertedString = "" + converted;
        int indexOfPeriod = convertedString.indexOf('.');
        String onlyTwoDecimal = convertedString.substring(0,Math.min(indexOfPeriod + 3, convertedString.length()));
        return Double.parseDouble(onlyTwoDecimal);
    }
}
