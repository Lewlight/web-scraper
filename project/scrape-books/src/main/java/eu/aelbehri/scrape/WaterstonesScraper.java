package eu.aelbehri.scrape;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import java.util.List;

/**
 * This class is used to scrape waterstones
 * @author Ayman El Behri
 */
public class WaterstonesScraper extends WebScraper{
    
    /**
    * This method is used to scrape waterstones.com using various key words.
    */
    @Override
    public void run(){
        //set up driver to get the data.
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);

        WebDriver driver = new ChromeDriver(options);

        for(String genre: getSearchGenres()){
            driver.get(String.format(getLink(), genre));
        
            // we scroll to the bottom twice for more data to load.
            JavascriptExecutor jse = (JavascriptExecutor)driver;
            jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
            try {
                Thread.sleep(getTimeout());
            }
            catch(Exception ex){
                ex.printStackTrace();
            }

            jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
            try {
                Thread.sleep(getTimeout());
            }
            catch(Exception ex){
                ex.printStackTrace();
            }

            //get all the products html blocks.
            List<WebElement> bookList = driver.findElements(By.className("book-preview"));
            for (WebElement book : bookList) {
                try{
                    String isbn = book.getAttribute("data-isbn");
                    if(!validateIsbn(isbn)) continue;
                    String imageLink = book.findElement(By.tagName("img")).getAttribute("src");
                    String link = book.findElement(By.className("title")).getAttribute("href");
                    String title = book.findElement(By.className("title")).getText();
                    String author = book.findElement(By.className("author")).getText();
                    String price = book.findElement(By.className("price")).getText();
                    String format = book.findElements(By.className("format")).get(1).getText().trim();

                    //create objs from data.
                    Book bookFound = new Book();
                    bookFound.setAuthor(removeIllegalCharacters(author.trim()));
                    bookFound.setGenre(genre);
                    bookFound.setTitle(removeIllegalCharacters(title.trim()));

                    Format formatFound = new Format();
                    formatFound.setIsbn(isbn.trim());
                    formatFound.setFormat(format.trim());

                    Product productFound = new Product();
                    productFound.setLink(link.trim());
                    productFound.setImageLink(imageLink.trim());
                    productFound.setWebsite("Wordery.com");
                    productFound.setIsbn(isbn.trim());
                    productFound.setPrice(filterPrice(price));
                    //send data to the database.
                    saveData(bookFound, formatFound, productFound);
                }
                catch(Exception ex){
                    //ignore the product if any exception occurs.
                    continue;
                }
            }
        }

        driver.quit();
    }
}
