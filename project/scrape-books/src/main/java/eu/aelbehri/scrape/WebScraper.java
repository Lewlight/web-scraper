package eu.aelbehri.scrape;

import org.hibernate.*;
import java.util.List;
/**
 * This is the parent class of all scrapers.
 * @author Ayman El Behri
 */
public class WebScraper extends Thread{
    private String link;
    private int timeout;
    private SessionFactory sessionFactory;
    private String[] searchGenres;
    
    /**
     * No argument constructor. It also sets standard timeout.
     */
    public WebScraper(){
        //default timeout value to allow website to load.
        timeout = 2000;
    }

    /**
     * @return the array of genres to search for.
     */
    public String[] getSearchGenres() {
        return searchGenres;
    }

    /**
     * This sets the genres to search for
     * @param searchGenres books genre .
     */
    public void setSearchGenres(String[] searchGenres) {
        this.searchGenres = searchGenres;
    }

    /**
     * @return The session factory used to access the database.
     */
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * This sets the SessionFactory to be used for the SQL connection
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * @return the link that is used for scraping this website.
     */
    public String getLink() {
        return link;
    }

    /**
     * This sets the search link.
     * @param link search link.
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return the timeout that allows time for the page to load.
     */
    public int getTimeout() {
        return timeout;
    }

    /**
     * This sets the timeout to allow page to load
     * @param timeout
     */
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }
    
    /**
     * This method checks for any duplicates of any of the columns we are trying
     * to insert into our database, before we actually save them to the database.
     * @param book the book we try to send to the database.
     * @param format the format type we try to send to the database.
     * @param product the product we found we try to send to the database.
     */
    public void saveData(Book book, Format format, Product product){
        // we get new session, then begin new transaction
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        // we check if the isbn and format found exist already
        List<Format> listFormats = session.createQuery(String.format("from Format where isbn=%s", format.getIsbn())).getResultList();
        //if it exists then we only need to save or update the product info since
        //since the format and book already exist.
        if(!listFormats.isEmpty()){
            session.saveOrUpdate(product);
        }
        else{
            //we search for the book we found from the book table.
            List<Book> listBooks = session.createQuery(String.format("from Book " +
                    "where upper(title) like '%%%s%%' and upper(author) LIKE '%%%s%%'",
                    book.getTitle().toUpperCase(), book.getAuthor().toUpperCase())).getResultList();
            //if it doesnt exist we save all three objects.
            if(listBooks.isEmpty()){
                //We set the book's imageLink before we save it.
                book.setImageLink(product.getImageLink());
                //This is the id of the book that was just saved into the database.
                int id = Integer.parseInt(session.save(book).toString());
                //we link it to the format, then save the rest.
                format.setId(id);
                session.save(format);
                session.saveOrUpdate(product);
            }
            else{
                //I will take the first book found, in case multiple books were found
                //I will ignore that for rising complexity.
                Book matchingBook = listBooks.get(0);
                format.setId(matchingBook.getId());
                session.save(format);
                session.saveOrUpdate(product);
            }
        }
        //end transaction.
        session.getTransaction().commit();
        session.close();
    }
    
    /**
     * This method removes the currency character from the beginning.
     * @param price to be filtered
     * @return filtered price
     */
    public double filterPrice(String price){
        return Double.parseDouble(price.substring(1));
    }
    
    /**
     * This method removes the apostrophe character from the string inputted.
     * @param data to be cleaned
     * @return cleaned string
     */
    public String removeIllegalCharacters(String data){
        return data.replace("\'", "");
    }
    
    /**
     * This method validates The Isbn extracted
     * @param isbn
     * @return if the Isbn is valid or not.
     */
    public boolean validateIsbn(String isbn){
        for(int i  =0; i < isbn.length(); i++){
            if(!Character.isDigit(isbn.charAt(i))){
                return false;
            }
        }
        return isbn.length() == 13;
    }
}
