package eu.aelbehri.scrape;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import java.util.List;

/**
 * This class is used to scrape wordery
 * @author Ayman El Behri
 */
public class WorderyScraper extends WebScraper{
    
    /**
     * No argument constructor it also sets up the search link.
     */
    public WorderyScraper(){
        setLink("https://wordery.com/search?viewBy=grid&resultsPerPage=100&term=%s&page=1&leadTime[]=any");
    }
    
    /**
    * This method is called to start scraping wordery.com with a list of keywords.
    * @author Ayman El Behri
    */
    @Override
    public void run(){
        //set up driver to get the data.
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(false);

        WebDriver driver = new ChromeDriver(options);
        for(String genre: getSearchGenres()){
            driver.get(String.format(getLink(), genre));
            JavascriptExecutor jse = (JavascriptExecutor)driver;
            //we scroll through the whole website slowly allowing the data to load.
            for(int i = 20; i > 0; i--){
                jse.executeScript(String.format("window.scrollTo(0, document.body.scrollHeight/%d)", i));
                try {
                    Thread.sleep(500);
                }
                catch(Exception ex){
                    ex.printStackTrace();
                }
            }

            //give more time for page to load
            try {
                Thread.sleep(getTimeout());
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
            //get all the products html blocks.
            List<WebElement> bookList = driver.findElements(By.className("o-book-list__book"));
            for (WebElement book : bookList) {
                try{
                    String imageLink = book.findElement(By.tagName("img")).getAttribute("src");
                    String link = book.findElement(By.className("c-book__title")).getAttribute("href");
                    link = filterLink(link);
                    String isbn = book.findElement(By.className("c-book")).getAttribute("data-isbn13");
                    if(!validateIsbn(isbn)) continue;
                    String title = book.findElement(By.className("c-book__title")).getText();
                    String author = book.findElement(By.className("c-book__by")).getText();
                    String price = book.findElement(By.className("c-book__price")).getText();
                    String format = book.findElement(By.className("c-book__meta")).getText(); //If necessary we can scrape the link of the game
                    //in case the imageLink took to long to load we skip this product.
                    if(imageLink == null){
                        continue;
                    }
                    //create objs from data
                    Book bookFound = new Book();
                    bookFound.setAuthor(removeIllegalCharacters(author.trim()));
                    bookFound.setGenre(genre);
                    bookFound.setTitle(removeIllegalCharacters(title.trim()));

                    Format formatFound = new Format();
                    formatFound.setIsbn(isbn.trim());
                    formatFound.setFormat(format.trim());

                    Product productFound = new Product();
                    productFound.setLink(link.trim());
                    productFound.setImageLink(imageLink.trim());
                    productFound.setWebsite("Wordery.com");
                    productFound.setIsbn(isbn.trim());
                    productFound.setPrice(filterPrice(price));
                    //send objs to database.
                    saveData(bookFound, formatFound, productFound);
                }
                catch(Exception ex){
                    //ignore product if any exception occurs.
                    System.out.println(ex.getMessage());
                    continue;
                }
            }
        }
        driver.quit();
    }
    
    /**
     * This method was added because the website is generating links
     * for products sort of randomly which leads to duplicate entries in
     * the product table, so we truncate the random chink from the link,
     * and we still end up with a functioning link.
     * @param link
     * @return truncated functioning link.
     */
    public String filterLink(String link){
        for (int i = 0; i < link.length(); i++){
            if(link.charAt(i) == '?'){
                return link.substring(0, i+1);
            }
        }
        return link;
    }
    
    /**
     * keeps relevant price value
     * @param price
     * @return price value.
     */
    @Override
    public double filterPrice(String price){
        String discounted = price.split(" ", 2)[0];
        return Double.parseDouble(discounted.substring(1));
    }
}
