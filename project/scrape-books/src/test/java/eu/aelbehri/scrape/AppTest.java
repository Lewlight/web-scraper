package eu.aelbehri.scrape;


import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class AppTest 
{
    @Test
    public void testIsbnValidation()
    {
        WebScraper scraper = new WebScraper();
        //We check limit cases and known mistakes in isbn (if there is a letter)
        String correctIsbn = "9999999999999";
        String tooLongIsbn = "9999999999999999";
        String incorrectIsbn = "9999aaaa99999";
        String emptyIsbn = "";
        assertTrue(scraper.validateIsbn(correctIsbn));
        assertFalse(scraper.validateIsbn(tooLongIsbn));
        assertFalse(scraper.validateIsbn(incorrectIsbn));
        assertFalse(scraper.validateIsbn(emptyIsbn));
    }
    
    @Test
    public void testPriceFilter()
    {
        WebScraper scraper = new WebScraper();
        String price = "£9.99";
        String priceWithoutDecimal = "£9";
        String priceWithWhiteSpace = "$9.99 ";
        assertEquals(9.99, scraper.filterPrice(price));
        assertEquals(9, scraper.filterPrice(priceWithoutDecimal));
        assertEquals(9.99, scraper.filterPrice(priceWithWhiteSpace));
    }
    
    @Test
    public void testShakespeareScraperPriceFilter()
    {
        ShakespeareScraper scraper = new ShakespeareScraper();
        //different prices to nake sure the conversion goes correctly
        String price = "100 €";
        String priceZero = "0 €";
        String priceWithDecimal = "10.1 €";
        assertEquals(85.0, scraper.filterPrice(price));
        assertEquals(0, scraper.filterPrice(priceZero));
        //we do the calculation here because double multiplication ends up with
        //8
        assertEquals(8.58, scraper.filterPrice(priceWithDecimal));
    }
    
    @Test
    public void testWorderyScraperPriceFilter()
    {
        WorderyScraper scraper = new WorderyScraper();        
        //we check if only the current price value(in double) is kept.
        String priceWithOldPrice = "£5.99 £80.80";
        String priceOnlyCurrent = "£5.99";
        String priceWithSpace = "£5.99 ";
        assertEquals(5.99, scraper.filterPrice(priceWithOldPrice));
        assertEquals(5.99, scraper.filterPrice(priceOnlyCurrent));
        assertEquals(5.99, scraper.filterPrice(priceWithSpace));
    }
    
    @Test
    public void testBookshopScraper()
    {
        BookshopScraper scraper = new BookshopScraper();
        //make sure the isbn is extracted correctly.
        String link = "bookshop.com/title/9999999999999";
        //some links may have just isbn.
        String linkWithoutTitle = "bookshop.com/9999999999999";
        assertEquals("9999999999999", scraper.getIsbn(link));
        assertEquals("9999999999999", scraper.getIsbn(linkWithoutTitle));
    }
}
