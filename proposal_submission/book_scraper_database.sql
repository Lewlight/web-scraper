CREATE DATABASE IF NOT EXISTS book_scraper;
USE book_scraper;

CREATE TABLE IF NOT EXISTS book (
	`id` INT NOT NULL AUTO_INCREMENT,
	`title` varchar(100) NOT NULL,
	`genre` varchar(100),
	`author` varchar(100) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `format` (
	`isbn` varchar(13) NOT NULL,
	`format` VARCHAR(50),
	`id` INT NOT NULL,
	PRIMARY KEY (`isbn`),
	FOREIGN KEY (id) REFERENCES book (id)
);


CREATE TABLE IF NOT EXISTS `product` (
	`link` VARCHAR(100) NOT NULL,
	`image_link` VARCHAR(100) NOT NULL,
	`price` FLOAT,
	`isbn` VARCHAR(13) NOT NULL,
	`website` VARCHAR(100),
	PRIMARY KEY (`link`),
	FOREIGN KEY (isbn) REFERENCES format (isbn)
);